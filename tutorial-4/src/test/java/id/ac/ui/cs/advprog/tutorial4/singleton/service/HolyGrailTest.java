package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    private HolyGrail holyGrail;

    @BeforeEach
    public void setUp() throws Exception{
    	holyGrail = new HolyGrail();
    }

    @Test
    public void testHolyWishIsNotNull(){
    	assertNotNull(holyGrail.getHolyWish());
    }

    @Test
    public void testMakeAWish(){
    	holyGrail.makeAWish("I wish...");
    	assertEquals(holyGrail.getHolyWish().getWish(), "I wish...");
    }
}
