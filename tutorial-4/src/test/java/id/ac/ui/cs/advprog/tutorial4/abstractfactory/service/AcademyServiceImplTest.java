package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    @BeforeEach
    public void setUp(){
    	academyService = new AcademyServiceImpl(new AcademyRepository());
    }

    @Test
    public void testAcademyCount(){
    	assertEquals(academyService.getKnightAcademies().size(), 2);
    }

    @Test
    public void testProduceKnight(){
    	academyService.produceKnight("Drangleic", "majestic");
    	assertEquals(academyService.getKnight().getName(), "Majestic Knight");

		academyService.produceKnight("Drangleic", "metal cluster");
    	assertEquals(academyService.getKnight().getName(), "Metal Cluster Knight");

    	academyService.produceKnight("Drangleic", "synthetic");
    	assertEquals(academyService.getKnight().getName(), "Synthetic Knight");    	

    	academyService.produceKnight("Lordran", "majestic");
    	assertEquals(academyService.getKnight().getName(), "Majestic Knight");

		academyService.produceKnight("Lordran", "metal cluster");
    	assertEquals(academyService.getKnight().getName(), "Metal Cluster Knight");

    	academyService.produceKnight("Lordran", "synthetic");
    	assertEquals(academyService.getKnight().getName(), "Synthetic Knight");
    }
}
