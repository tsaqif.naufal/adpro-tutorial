package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {

    public String attack(){
    	return "Launch an attack with sword!";
    }

    public String getType(){
    	return "Sword";	
    }
}
