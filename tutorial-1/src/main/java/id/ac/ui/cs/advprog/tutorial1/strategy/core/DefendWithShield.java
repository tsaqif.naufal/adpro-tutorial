package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {

	public String defend(){
		return "Do defense with shield!";
	}

    public String getType(){
    	return "Shield";	
    }
}
