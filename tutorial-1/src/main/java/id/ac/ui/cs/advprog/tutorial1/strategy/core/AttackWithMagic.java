package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {

	public String attack(){
    	return "Launch an attack with magic!";
    }

	public String getType(){
    	return "Magic";	
    }
}
