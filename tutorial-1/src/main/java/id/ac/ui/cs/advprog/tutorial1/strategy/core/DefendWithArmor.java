package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
	
	public String defend(){
		return "Do defense with armor!";
	}

    public String getType(){
    	return "Armor";
    }
}
