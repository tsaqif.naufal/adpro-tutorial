package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
	private String name;
	private String role;
	private List<Member> childMembers;

	public OrdinaryMember(String name, String role){
		this.name = name;
		this.role = role;
		childMembers = new ArrayList<>();
	}

    public String getName(){
    	return this.name;
    }

    public String getRole(){
    	return this.role;
    }

    public void addChildMember(Member member){
    	// nothing to do here
    }

    public void removeChildMember(Member member){
    	// nothing to do here
    }

    public List<Member> getChildMembers(){
    	return this.childMembers;
    }
}
