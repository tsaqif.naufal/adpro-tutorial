package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;


import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChaosUpgradeTest {

    private ChaosUpgrade chaosUpgrade;
    @BeforeEach
    public void setUp(){
        chaosUpgrade = new ChaosUpgrade(new Gun());
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals(chaosUpgrade.getName(), "Gun");
    }

    @Test
    public void testGetMethodWeaponDescription(){
        assertTrue(chaosUpgrade.getDescription().contains("Chaos Upgrade"));
    }

    @Test
    public void testMethodGetWeaponValue(){
        int value = chaosUpgrade.getWeaponValue();
        assertTrue(value >= 70 && value <= 75);
    }
}
