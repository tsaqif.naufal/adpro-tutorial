package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class GunTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Gun();
    }

    @Test
    public void testMethodGetWeaponName(){
        assertEquals(weapon.getName(), "Gun");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        assertEquals(weapon.getDescription(), "Automatic Gun");
    }

    @Test
    public void testMethodGetWeaponValue(){
        assertEquals(weapon.getWeaponValue(), 20);
    }
}
