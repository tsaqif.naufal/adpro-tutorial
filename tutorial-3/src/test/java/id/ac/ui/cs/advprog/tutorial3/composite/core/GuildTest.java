package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        int oldSize = guild.getMemberList().size();

        Member newMember = new PremiumMember("Joko", "Adviser");
        guild.addMember(guildMaster, newMember);
        
        assertEquals(guild.getMemberList().size(), oldSize + 1);
        assertEquals(guild.getMember("Joko", "Adviser"), newMember);
    }

    @Test
    public void testMethodRemoveMember() {
        Member newMember = new PremiumMember("Bowo", "Adviser");
        guild.addMember(guildMaster, newMember);
        int oldSize = guild.getMemberList().size();

        guild.removeMember(guildMaster, newMember);
        
        assertEquals(guild.getMemberList().size(), oldSize - 1);
        assertEquals(guild.getMember("Bowo", "Adviser"), null);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        Member newMember = new PremiumMember("Panji", "Adviser");
        guild.addMember(guildMaster, newMember);
        assertEquals(guild.getMember("Panji", "Adviser"), newMember);
    }
}
