package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName(){
        assertEquals(member.getName(), "Wati");
    }

    @Test
    public void testMethodGetRole() {
        assertEquals(member.getRole(), "Gold Merchant");
    }

    @Test
    public void testMethodAddChildMember() {
        int oldSize = member.getChildMembers().size();
        member.addChildMember(new PremiumMember("Joko", "Adviser"));
        assertEquals(member.getChildMembers().size(), oldSize + 1);
    }

    @Test
    public void testMethodRemoveChildMember() {
        Member newMember = new PremiumMember("Bowo", "Adviser");
        member.addChildMember(newMember);
        int oldSize = member.getChildMembers().size();

        member.removeChildMember(newMember);
        assertEquals(member.getChildMembers().size(), oldSize - 1);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        member.addChildMember(new PremiumMember("Panji", "Adviser"));
        assertEquals(member.getChildMembers().size(), 1);

        member.addChildMember(new OrdinaryMember("Ridwan", "Merchant"));
        assertEquals(member.getChildMembers().size(), 2);

        member.addChildMember(new OrdinaryMember("Ari", "Merchant"));
        assertEquals(member.getChildMembers().size(), 3);

        member.addChildMember(new OrdinaryMember("Ata", "Merchant"));
        assertEquals(member.getChildMembers().size(), 3);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        Member master = new PremiumMember("Eko", "Master");

        master.addChildMember(new PremiumMember("Panji", "Adviser"));
        assertEquals(master.getChildMembers().size(), 1);

        master.addChildMember(new OrdinaryMember("Ridwan", "Merchant"));
        assertEquals(master.getChildMembers().size(), 2);

        master.addChildMember(new OrdinaryMember("Ari", "Merchant"));
        assertEquals(master.getChildMembers().size(), 3);

        master.addChildMember(new OrdinaryMember("Ata", "Merchant"));
        assertEquals(master.getChildMembers().size(), 4);
    }
}
